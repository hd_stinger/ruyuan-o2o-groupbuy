package com.ruyuan.o2o.groupbuy.item.admin.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.evaluate.model.EvaluateModel;
import com.ruyuan.o2o.groupbuy.evaluate.service.EvaluateService;
import com.ruyuan.o2o.groupbuy.item.admin.dao.ItemDao;
import com.ruyuan.o2o.groupbuy.item.admin.dto.ItemDto;
import com.ruyuan.o2o.groupbuy.item.model.ItemModel;
import com.ruyuan.o2o.groupbuy.item.service.ItemService;
import com.ruyuan.o2o.groupbuy.item.vo.ItemVO;
import com.ruyuan.o2o.groupbuy.promotions.model.PromotionsModel;
import com.ruyuan.o2o.groupbuy.promotions.service.PromotionsService;
import com.ruyuan.o2o.groupbuy.store.model.StoreModel;
import com.ruyuan.o2o.groupbuy.store.service.StoreService;
import com.ruyuan.o2o.groupbuy.store.vo.StoreVO;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理商品服务的增删改查service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = ItemService.class, cluster = "failfast", loadbalance = "roundrobin")
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDao itemDao;

    @Reference(version = "1.0.0", interfaceClass = PromotionsService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private PromotionsService promotionsService;

    @Reference(version = "1.0.0", interfaceClass = StoreService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private StoreService storeService;

    @Reference(version = "1.0.0", interfaceClass = EvaluateService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private EvaluateService evaluateService;

    /**
     * 创建商品
     *
     * @param itemVO
     */
    @Override
    public void save(ItemVO itemVO) {
        ItemModel itemModel = new ItemModel();
        BeanMapper.copy(itemVO, itemModel);
        //  上传商品图片 获得图片服务地址
        itemModel.setItemImg1("http://image1");
        itemModel.setItemImg2("http://image2");
        itemModel.setItemImg3("http://image3");
        itemModel.setItemImg4("http://image4");
        itemModel.setItemImg5("http://image5");

        itemModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        itemModel.setUpdateTime(TimeUtil.format(System.currentTimeMillis()));
        itemDao.save(itemModel);
    }

    /**
     * 更新商品
     *
     * @param itemVO
     */
    @Override
    public void update(ItemVO itemVO) {
        ItemModel itemModel = new ItemModel();
        BeanMapper.copy(itemVO, itemModel);
        itemModel.setUpdateTime(TimeUtil.format(System.currentTimeMillis()));
        itemDao.update(itemModel);
    }

    /**
     * 下架商品
     *
     * @param itemId
     */
    @Override
    public void offLine(Integer itemId) {
        itemDao.offLine(itemId);
    }

    /**
     * 上架商品
     *
     * @param itemId
     */
    @Override
    public void onLine(Integer itemId) {
        itemDao.onLine(itemId);
    }

    /**
     * 分页查询商品
     *
     * @return
     */
    @Override
    public List<ItemVO> listByPage(Integer pageNum, Integer pageSize) {
        List<ItemVO> itemVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<ItemModel> itemModels = itemDao.listByPage();

        for (ItemModel itemModel : itemModels) {
            ItemVO itemVO = new ItemVO();
            BeanMapper.copy(itemModel, itemVO);
            itemVoS.add(itemVO);
        }

        return itemVoS;
    }

    /**
     * 根据id查询商品
     *
     * @param itemId 商品id
     * @return
     */
    @Override
    public ItemVO findById(Integer itemId) {
        ItemModel itemModel = itemDao.findById(itemId);
        ItemVO itemVO = new ItemVO();
        BeanMapper.copy(itemModel, itemVO);
        return itemVO;
    }

    /**
     * 删除商品
     *
     * @param itemId
     */
    @Override
    public void delete(Integer itemId) {
        itemDao.delete(itemId);
    }

    /**
     * 用户端商品详情
     *
     * @param itemId 商品id
     * @return 商品json信息
     */
    @Override
    public String findDetailById(Integer itemId) {
        ItemModel itemModel = itemDao.findById(itemId);

        PromotionsModel runningPromotion = promotionsService.findRunningPromotion();

        StoreVO storeVO = storeService.findById(itemModel.getStoreId());
        StoreModel storeModel = new StoreModel();
        BeanMapper.copy(storeVO, storeModel);

        EvaluateModel evaluateModel = evaluateService.findByItemId(itemId);

        ItemDto itemDto = new ItemDto();
        itemDto.setItemModel(itemModel);
        itemDto.setPromotionsModel(runningPromotion);
        itemDto.setStoreModel(storeModel);
        itemDto.setEvaluateModel(evaluateModel);

        return JSONObject.toJSONString(itemDto);
    }
}
