package com.ruyuan.o2o.groupbuy.item.http;

import com.ruyuan.o2o.groupbuy.item.service.ItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

/**
 * 用户选择商品跳转商品详情页
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/item/detail")
@Slf4j
@Api(tags = "用户端商品详情")
public class ItemHttpController {

    @Reference(version = "1.0.0", interfaceClass = ItemService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private ItemService itemService;

    /**
     * 商品详情
     *
     * @param itemId 前台商品id
     * @return
     */
    @GetMapping("/{itemId}")
    @ApiOperation("查询商品详情")
    public String save(@PathVariable("itemId") Integer itemId) {
        return itemService.findDetailById(itemId);
    }
}
