package com.ruyuan.o2o.groupbuy.item;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 商品服务启动类
 *
 * @author ming qian
 */
@SpringBootApplication
@MapperScan("com.ruyuan.o2o.groupbuy.item.admin.dao")
@EnableDiscoveryClient
@Configuration
@EnableSwagger2
public class ItemMain {
    public static void main(String[] args) {
        SpringApplication.run(ItemMain.class, args);
    }

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder().title("商品服务").description("商品服务接口文档").version("1.0").build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.ruyuan.o2o.groupbuy.item"))
                .paths(PathSelectors.any())
                .build();
    }
}
