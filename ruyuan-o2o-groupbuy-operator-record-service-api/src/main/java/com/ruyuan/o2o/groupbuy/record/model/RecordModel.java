package com.ruyuan.o2o.groupbuy.record.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 消费者用户操作记录服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class RecordModel implements Serializable {
    private static final long serialVersionUID = 103824099348264851L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 消费者用户id
     */
    private Integer userId;

    /**
     * 门店id
     */
    private Integer storeId;

    /**
     * 操作记录 0 下单 1 支付完成 2 到店核销 3 货物签收 4 投诉门店 5 获赠积分 6 参与抽奖
     */
    private Integer operateRecord;

    /**
     * 创建时间
     */
    private String createTime;
}