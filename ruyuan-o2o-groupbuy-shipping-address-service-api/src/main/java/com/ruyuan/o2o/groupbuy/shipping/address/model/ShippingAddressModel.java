package com.ruyuan.o2o.groupbuy.shipping.address.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户收货地址服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class ShippingAddressModel implements Serializable {
    private static final long serialVersionUID = -7784372538970708055L;

    /**
     * 主键id
     */
    private Integer id;
    /**
     * 消费者用户id
     */
    private Integer userId;
    /**
     * 省id
     */
    private Integer provinceId;
    /**
     * 市id
     */
    private Integer cityId;
    /**
     * 区id
     */
    private Integer districtId;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 详细地址
     */
    private String detailAddress;
}