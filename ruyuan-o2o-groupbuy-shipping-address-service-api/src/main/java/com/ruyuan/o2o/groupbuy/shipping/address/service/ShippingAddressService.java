package com.ruyuan.o2o.groupbuy.shipping.address.service;


import com.ruyuan.o2o.groupbuy.shipping.address.vo.ShippingAddressVO;

/**
 * 收货地址服务service组件接口
 *
 * @author ming qian
 */
public interface ShippingAddressService {
    /**
     * 保存收货地址
     *
     * @param shippingAddressVO
     */
    void save(ShippingAddressVO shippingAddressVO);
}
