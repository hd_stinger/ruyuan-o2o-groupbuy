package com.ruyuan.o2o.groupbuy.batch.service;

/**
 * 批处理任务service组件接口
 *
 * @author ming qian
 */
public interface BatchService {
    /**
     * 执行批处理
     *
     * @param
     */
    void batch();
}
