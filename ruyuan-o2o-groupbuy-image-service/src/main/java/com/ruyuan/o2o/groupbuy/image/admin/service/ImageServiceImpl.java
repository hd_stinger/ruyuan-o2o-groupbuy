package com.ruyuan.o2o.groupbuy.image.admin.service;

import com.ruyuan.o2o.groupbuy.image.service.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;

/**
 * 图片服务
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = ImageService.class, cluster = "failfast", loadbalance = "roundrobin")
@Slf4j
public class ImageServiceImpl implements ImageService {

    /**
     * 上传图片
     *
     * @param
     */
    @Override
    public String uploadImage() {
        //  调用小文件系统上传图片
        log.info("上传图片完成,图片地址:{}", "http://image.com/xx/image1/");
        return "http://image.com/xx/image1/";
    }
}
