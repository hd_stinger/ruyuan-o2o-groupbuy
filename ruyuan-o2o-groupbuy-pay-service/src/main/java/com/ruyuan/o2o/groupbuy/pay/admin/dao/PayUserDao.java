package com.ruyuan.o2o.groupbuy.pay.admin.dao;

import com.ruyuan.o2o.groupbuy.pay.model.PayUserModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户消费记录信息服务DAO
 *
 * @author ming qian
 */
@Repository
public interface PayUserDao {

    /**
     * 分页查询支付流水信息
     *
     * @return
     */
    List<PayUserModel> listByPage();
}