package com.ruyuan.o2o.groupbuy.overage.admin.dao;

import com.ruyuan.o2o.groupbuy.overage.model.OverageModel;
import org.springframework.stereotype.Repository;

/**
 * 充值服务DAO
 *
 * @author ming qian
 */
@Repository
public interface OverageDao {
    /**
     * 创建充值
     *
     * @param overageModel 充值服务model
     */
    void save(OverageModel overageModel);

    /**
     * 查询余额
     *
     * @param overageModel 充值服务model
     * @return 消费者在门店总余额
     */
    Double findOverage(OverageModel overageModel);
}