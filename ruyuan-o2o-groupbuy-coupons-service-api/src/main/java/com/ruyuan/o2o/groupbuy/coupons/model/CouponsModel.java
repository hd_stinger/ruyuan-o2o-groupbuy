package com.ruyuan.o2o.groupbuy.coupons.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 优惠券服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class CouponsModel implements Serializable {
    private static final long serialVersionUID = -1353931206956532112L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 优惠券id
     */
    private Integer couponsId;

    /**
     * 优惠券名称
     */
    private Integer couponsName;

    /**
     * 优惠券类型 0 满减 1 折扣
     */
    private Integer couponsType;

    /**
     * 满减券满足金额
     */
    private Double couponsFullPrice;

    /**
     * 满减券扣减金额
     */
    private Double couponsDeductPrice;

    /**
     * 折扣券打折比例
     */
    private Double couponsDiscountPrice;

    /**
     * 优惠券有效开始时间
     */
    private String couponStartTime;

    /**
     * 优惠券有效结束时间
     */
    private String couponEndTime;

    /**
     * 创建时间
     */
    private String createTime;
}