package com.ruyuan.o2o.groupbuy.account.service;

import com.ruyuan.o2o.groupbuy.account.vo.RoleAuthVO;
import com.ruyuan.o2o.groupbuy.account.vo.RoleVO;

import java.util.List;

/**
 * 角色管理service组件接口
 *
 * @author ming qian
 */
public interface RoleService {
    /**
     * 创建角色
     *
     * @param roleVO 角色vo
     */
    void save(RoleVO roleVO);

    /**
     * 更新角色
     *
     * @param roleVO 角色vo
     */
    void update(RoleVO roleVO);

    /**
     * 分页查询
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<RoleVO> listByPage(Integer pageNum, Integer pageSize);

    /**
     * 根据id查询角色
     *
     * @param roleId 角色id
     * @return 角色vo
     */
    RoleVO findById(Integer roleId);

    /**
     * 角色绑定权限
     *
     * @param roleAuthVO 前台表单
     * @return 绑定结果
     */
    boolean roleBindAuth(RoleAuthVO roleAuthVO);
}
