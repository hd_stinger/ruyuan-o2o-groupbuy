package com.ruyuan.o2o.groupbuy.account.admin.controller;

import com.ruyuan.o2o.groupbuy.account.vo.RoleAuthVO;
import com.ruyuan.o2o.groupbuy.account.vo.RoleVO;
import com.ruyuan.o2o.groupbuy.account.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商户角色
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/role")
@Slf4j
@Api(tags = "商户角色")
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 创建角色
     *
     * @param roleVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("创建角色")
    public Boolean save(@RequestBody RoleVO roleVO) {
        roleService.save(roleVO);
        log.info("创建角色:{} 完成", roleVO.getRoleName());
        return Boolean.TRUE;
    }

    /**
     * 更新角色
     *
     * @param roleVO 前台表单
     * @return
     */
    @PostMapping("/update")
    @ApiOperation("更新角色")
    public Boolean update(@RequestBody RoleVO roleVO) {
        roleService.update(roleVO);
        log.info("更新角色:{} 完成", roleVO.getRoleName());
        return Boolean.TRUE;
    }


    /**
     * 分页查询角色列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询角色列表")
    public List<RoleVO> page(Integer pageNum, Integer pageSize) {
        List<RoleVO> roleVoS = roleService.listByPage(pageNum, pageSize);
        return roleVoS;
    }

    /**
     * 查询角色详情
     *
     * @param roleId 角色id
     * @return
     */
    @GetMapping("/{roleId}")
    @ApiOperation("查询角色详情")
    public RoleVO page(@PathVariable("roleId") Integer roleId) {
        RoleVO roleVO = roleService.findById(roleId);
        return roleVO;
    }

    /**
     * 角色绑定权限
     * 前台权限id可多选，一个角色可绑定多个权限id（auth_id）
     *
     * @param roleAuthVO 前台表单
     * @return
     */
    @PostMapping("/bind/auth")
    @ApiOperation("角色绑定权限")
    public String bindAuth(@RequestBody RoleAuthVO roleAuthVO) {
        Integer roleId = roleAuthVO.getRoleId();
        String authIds = roleAuthVO.getAuthIds();

        if (!roleService.roleBindAuth(roleAuthVO)) {
            return "fail";
        }
        log.info("角色:{}, 权限:{} 绑定成功", roleId, authIds);
        return "success";
    }
}
