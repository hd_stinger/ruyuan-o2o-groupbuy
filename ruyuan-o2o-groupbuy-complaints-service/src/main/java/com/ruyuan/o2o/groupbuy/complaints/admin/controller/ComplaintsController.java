package com.ruyuan.o2o.groupbuy.complaints.admin.controller;

import com.ruyuan.o2o.groupbuy.complaints.service.ComplaintsService;
import com.ruyuan.o2o.groupbuy.complaints.vo.ComplaintsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 投诉管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/complaints")
@Slf4j
@Api(tags = "投诉管理")
public class ComplaintsController {

    @Reference(version = "1.0.0", interfaceClass = ComplaintsService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private ComplaintsService complaintsService;

    /**
     * 接收投诉
     *
     * @param complaintsVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("接收投诉")
    public Boolean save(@RequestBody ComplaintsVO complaintsVO) {
        complaintsService.save(complaintsVO);
        log.info("接收投诉完成");
        return Boolean.TRUE;
    }
}
