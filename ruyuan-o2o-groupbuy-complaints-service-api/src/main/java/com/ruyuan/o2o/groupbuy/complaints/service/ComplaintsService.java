package com.ruyuan.o2o.groupbuy.complaints.service;


import com.ruyuan.o2o.groupbuy.complaints.vo.ComplaintsVO;

/**
 * 投诉服务service组件接口
 *
 * @author ming qian
 */
public interface ComplaintsService {
    /**
     * 接收投诉
     *
     * @param complaintsVO
     */
    void save(ComplaintsVO complaintsVO);
}
