package com.ruyuan.o2o.groupbuy.batch.admin.service;

import com.ruyuan.o2o.groupbuy.batch.service.BatchService;
import com.ruyuan.o2o.groupbuy.excel.service.ExcelService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;

/**
 * 批处理服务
 *
 * @author ming qian
 */
@Service(
    version = "1.0.0",
    interfaceClass = BatchService.class,
    cluster = "failfast",
    loadbalance = "roundrobin")
public class BatchServiceImpl implements BatchService {

  @Reference(
      version = "1.0.0",
      interfaceClass = ExcelService.class,
      cluster = "failfast",
      loadbalance = "roundrobin")
  private ExcelService excelService;

  @Override
  public void batch() {
    excelService.excel();
  }
}
