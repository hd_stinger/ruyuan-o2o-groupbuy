package com.ruyuan.o2o.groupbuy.evaluate.http;

import com.ruyuan.o2o.groupbuy.evaluate.service.EvaluateService;
import com.ruyuan.o2o.groupbuy.evaluate.vo.EvaluateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 评价管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/evaluate")
@Slf4j
@Api(tags = "评价管理")
public class EvaluateHttpController {

    @Reference(version = "1.0.0", interfaceClass = EvaluateService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private EvaluateService evaluateService;

    /**
     * 用户评价
     *
     * @param evaluateVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("用户评价")
    public Boolean save(@RequestBody EvaluateVO evaluateVO) {
        evaluateService.save(evaluateVO);
        log.info("评价完成");
        return Boolean.TRUE;
    }
}
