package com.ruyuan.o2o.groupbuy.code.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 券码服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "券码服务VO类")
@Getter
@Setter
@ToString
public class CodeVO implements Serializable {
    private static final long serialVersionUID = 1125117384154171705L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 支付id
     */
    @ApiModelProperty("支付id")
    private Integer payId;

    /**
     * 门店id
     */
    @ApiModelProperty("门店id")
    private Integer storeId;

    /**
     * 数字券码
     */
    @ApiModelProperty("数字券码")
    private Integer digitalCode;

    /**
     * 券码状态 0 未核销 1 已核销
     */
    @ApiModelProperty("券码状态 0 未核销 1 已核销")
    private Integer codeStatus;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private String updateTime;

    /**
     * 二维码地址
     */
    @ApiModelProperty("二维码地址")
    private String qrCodeImg;
}