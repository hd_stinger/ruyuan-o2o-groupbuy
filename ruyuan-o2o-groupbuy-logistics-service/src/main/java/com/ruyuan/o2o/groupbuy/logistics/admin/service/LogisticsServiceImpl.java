package com.ruyuan.o2o.groupbuy.logistics.admin.service;

import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.logistics.admin.dao.LogisticsDao;
import com.ruyuan.o2o.groupbuy.logistics.model.LogisticsModel;
import com.ruyuan.o2o.groupbuy.logistics.service.LogisticsService;
import com.ruyuan.o2o.groupbuy.logistics.vo.LogisticsVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 处理物流配送服务service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = LogisticsService.class, cluster = "failfast", loadbalance = "roundrobin")
public class LogisticsServiceImpl implements LogisticsService {

    @Autowired
    private LogisticsDao logisticsDao;

    /**
     * 保存物流配送信息
     *
     * @param logisticsVO
     */
    @Override
    public void save(LogisticsVO logisticsVO) {
        LogisticsModel logisticsModel = new LogisticsModel();
        BeanMapper.copy(logisticsVO, logisticsModel);
        logisticsModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        logisticsModel.setUpdateTime(TimeUtil.format(System.currentTimeMillis()));
        logisticsDao.save(logisticsModel);
    }
}
