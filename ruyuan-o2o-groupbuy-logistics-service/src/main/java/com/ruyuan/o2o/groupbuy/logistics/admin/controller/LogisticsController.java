package com.ruyuan.o2o.groupbuy.logistics.admin.controller;

import com.ruyuan.o2o.groupbuy.logistics.service.LogisticsService;
import com.ruyuan.o2o.groupbuy.logistics.vo.LogisticsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 物流配送管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/logistic")
@Slf4j
@Api(tags = "物流配送管理")
public class LogisticsController {

    @Reference(version = "1.0.0", interfaceClass = LogisticsService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private LogisticsService logisticsService;

    /**
     * 保存物流配送信息
     *
     * @param logisticsVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("保存物流配送信息")
    public Boolean save(@RequestBody LogisticsVO logisticsVO) {
        logisticsService.save(logisticsVO);
        log.info("保存物流配送信息完成");
        return Boolean.TRUE;
    }
}
