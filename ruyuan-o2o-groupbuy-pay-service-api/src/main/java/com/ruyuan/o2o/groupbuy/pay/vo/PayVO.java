package com.ruyuan.o2o.groupbuy.pay.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 支付服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "支付服务VO类")
@Getter
@Setter
@ToString
public class PayVO implements Serializable {
    private static final long serialVersionUID = -4581000535502898220L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 订单id
     */
    @ApiModelProperty("订单id")
    private String orderId;

    /**
     * 支付id
     */
    @ApiModelProperty("支付id")
    private Integer payId;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 门店id
     */
    @ApiModelProperty("门店id")
    private Integer storeId;

    /**
     * 支付状态 0 支付成功 1 支付失败
     */
    @ApiModelProperty("支付状态 0 支付成功 1 支付失败")
    private Integer payStatus;

    /**
     * 支付渠道 0 会员余额 1 银联卡 2 支付宝 3 微信
     */
    @ApiModelProperty("支付渠道 0 会员余额 1 银联卡 2 支付宝 3 微信")
    private Integer payChannel;

    /**
     * 支付时间
     */
    @ApiModelProperty("支付时间")
    private String payTime;
}