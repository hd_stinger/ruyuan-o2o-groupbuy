package com.ruyuan.o2o.groupbuy.user.service;

import com.ruyuan.o2o.groupbuy.user.vo.UserVO;

/**
 * 消费者用户服务service组件接口
 *
 * @author ming qian
 */
public interface UserService {
    /**
     * 用户注册
     *
     * @param userVO
     */
    void save(UserVO userVO);

    /**
     * 查询用户手机号
     *
     * @param userId 消费者用户id
     * @return 用户手机号
     */
    String findPhoneById(Integer userId);
}
