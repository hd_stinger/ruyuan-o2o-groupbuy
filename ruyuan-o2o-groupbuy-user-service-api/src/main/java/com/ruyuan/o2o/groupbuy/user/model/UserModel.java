package com.ruyuan.o2o.groupbuy.user.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 消费者会员用户服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class UserModel implements Serializable {
    private static final long serialVersionUID = -116551318656749304L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 消费者用户id
     */
    private Integer userId;

    /**
     * 用户手机号
     */
    private String userPhone;

    /**
     * 门店id
     */
    private Integer storeId;

    /**
     * 会员状态 0 未充值注册成为会员 1 已充值注册成为会员
     */
    private Integer memberStatus;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;
}