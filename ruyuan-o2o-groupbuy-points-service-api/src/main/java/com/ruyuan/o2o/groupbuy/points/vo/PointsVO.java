package com.ruyuan.o2o.groupbuy.points.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 积分服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "积分服务VO类")
@Getter
@Setter
@ToString
public class PointsVO implements Serializable {
    private static final long serialVersionUID = 4525093831229680731L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 本次增加积分
     */
    @ApiModelProperty("本次增加积分")
    private Integer pointsAdd;

    /**
     * 总积分
     */
    @ApiModelProperty("总积分")
    private Integer pointsTotal;

    /**
     * 支付id
     */
    @ApiModelProperty("支付id")
    private Integer payId;
}